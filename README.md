Logiciel d'auto import de fichier au format .WDX.

L'objectif de ce programme est de le placer à la racine du répertoire Winbiz et 
de l'exécuter de manière récurente de sorte à tenir à jour la DB Winbiz avec les fichiers WDX générer.

La configuration est pour l'heure indiquée en dur dans la fichier "program.cs" et contient:
	- Le chemin vers le répertoire contenant les fichiers wdx
	- Le chemin de la BD Winbiz
	- L'exercice comptable en cours
	- Le numéro de dossier de la société
	- Le chemin vers l'exécutable du logiciel Winbiz
	
Le programme intègre plusieurs DLL:
- Supermédiator (2.9), pour l'xécution de l'importation au sein de Winbiz
- System.IO.FileSystem (4.3.0), afin de consulter les fichiers présents dans le répertoire indiquer
