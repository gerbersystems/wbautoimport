﻿using System;
using System.Collections.Generic;
using System.IO;
using WbAutoImport.Repositories;

namespace WbAutoImport
{
    class Program
    {
        private static string WdxFormat = "*.wdx";
        private static string BackupFormat = "backup";

        //TODO acces to SQL ton find theses informations
        private static string wdxPath = @"\\tshosting09\e$\168\winbiz_db\wdxLogs";
        private static string wbPath = @"\\tshosting09\e$\168\winbiz_db";
        private static string wbCompany = "6";
        private static string wbYear = "2019";
        private static string wbExe = @"C:\Program Files (x86)\La Gestion Electronique SA\WinBIZ 9.0\WinBIZ.exe";
        

        static void Main(string[] args)
        {
            Console.WriteLine("Process running...");

            //Get WB informations from config file

            //Get list of all WDX to import
            List<string> wdxFiles = DirectoryRepository.GetFiles(wdxPath, WdxFormat, BackupFormat);

            //Execute import of all files
            foreach (string wdxFile in wdxFiles)
            {
                if (wdxFile != null)
                    WBAutoImport.AutoImportWdxFile(wdxPath, wdxFile, wbExe, wbPath, wbCompany, wbYear);
            }

            Console.WriteLine("Process Done !...");
            Console.ReadLine();

        }


    }
}
