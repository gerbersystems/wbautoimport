﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using VfpEntityFrameworkProvider;
using System.Threading;

namespace WbAutoImport
{
    public static class WBAutoImport
    {
        public static void AutoImportWdxFile(string pathWdxFolder, string wdxName, string winbizPath, string winbizDB, string winbizFolder, string winbizYear)
        {
            // Create Winbiz process and auto import
            Process winbizImportProcess = new Process();
            winbizImportProcess.StartInfo = new ProcessStartInfo(
                winbizPath, $"dbfolder={winbizDB} company={winbizFolder} year={winbizYear} command=DOCUMENTIMPORTNSI()");
            winbizImportProcess.Start();


            // Wait for the process to finish
            /*
            while (winbizImportProcess.Responding)
            {
                Thread.Sleep(1000);
            }
            */


        }
    }
}
