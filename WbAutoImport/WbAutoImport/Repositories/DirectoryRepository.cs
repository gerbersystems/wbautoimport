﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace WbAutoImport.Repositories
{
    static class DirectoryRepository
    {
        public static List<string> GetFiles(string path, string fileFormat, string BackupFormat)
        {
            List<string> wdxFiles = new List<string>();
            try
            {
                string[] dirs = Directory.GetFiles(path, fileFormat);
                
                Console.WriteLine("The number of files ending with .wdx is {0}.", dirs.Length);
                foreach (string dir in dirs)
                {
                    if (!dir.Contains(BackupFormat))
                        wdxFiles.Add(dir);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }

            return wdxFiles;
        }
    }
}
